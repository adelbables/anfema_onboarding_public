from datetime import datetime

import requests
from celery import shared_task
from django.http import HttpResponseBadRequest

from anfema_projects.models import AnfemaProject


@shared_task
def fetch_projects_older_than(older_than):
    try:
        older_than_date = datetime.strptime(older_than, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        return HttpResponseBadRequest("Invalid 'X-OLDER-THAN' format. Expected format: 'YYYY-MM-DD HH:MM:SS'.")

    projects_to_update = AnfemaProject.objects.filter(updated_at__lt=older_than_date)

    response = requests.get('https://www.anfe.ma/api/v2/projects/?format=json&locale=en')
    data = response.json()
    for project_data in data['items']:
        project_id = project_data['id']
        project = projects_to_update.filter(id=project_id).first()
        if project:
            project.title = project_data['title']
            project.meta_first_published_at = project_data['meta']['first_published_at']
            project.client = project_data['client']
            project.subtitle = project_data['subtitle']
            project.brand_main_color = project_data['brand_main_colour']
            project.save()


@shared_task
def fetch_projects():
    response = requests.get('https://www.anfe.ma/api/v2/projects/?format=json&locale=en')
    data = response.json()
    for project_data in data['items']:
        project = AnfemaProject(
            id=project_data['id'],
            title=project_data['title'],
            meta_first_published_at=project_data['meta']['first_published_at'],
            client=project_data['client'],
            subtitle=project_data['subtitle'],
            brand_main_color=project_data['brand_main_colour']
        )
        project.save()
