from django.contrib import admin
from .models import AnfemaProject


# Register your models here.
@admin.register(AnfemaProject)
class AnfemaProjectAdmin(admin.ModelAdmin):
    pass
