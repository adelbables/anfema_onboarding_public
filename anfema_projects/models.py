import datetime

from django.db import models


# Create your models here.
class AnfemaProject(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=255)
    meta_first_published_at = models.DateTimeField()
    client = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255)
    brand_main_color = models.CharField(max_length=7)  # Assuming the color is stored in hex format, e.g., #RRGGBB
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.updated_at = datetime.datetime.now()
        super().save(*args, **kwargs)
