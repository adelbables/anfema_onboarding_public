from django.http import HttpResponseNotAllowed
from django.http import JsonResponse, HttpResponse
from django.views import View

from anfema_projects.models import AnfemaProject
from anfema_projects.tasks import fetch_projects_older_than, fetch_projects


# Create your views here.
class LastUpdateView(View):
    def get(self, request):
        latest_project = AnfemaProject.objects.latest('updated_at')
        response_data = {
            'last_updated_at': latest_project.updated_at,
        }
        return JsonResponse(response_data)


class PerformUpdateView(View):
    def post(self, request):
        older_than = request.headers.get('X-OLDER-THAN')
        if older_than:
            fetch_projects_older_than.delay(older_than)
        else:
            fetch_projects.delay()

        return HttpResponse(status=202)

    def get(self, request):
        return HttpResponseNotAllowed("Not Allowed")
